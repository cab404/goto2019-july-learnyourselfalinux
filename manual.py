#!/bin/env python3
import os
import sys
import argparse
import json
import tempfile
import textwrap

from typing import NamedTuple, Callable, Optional, List

_state_file = f"{os.getenv('HOME')}/.config/.less.bin"
_init_state = {"solved": []}
state = {}

# Cause we won't install pycrypto just for that
# noinspection PyShadowingNames
def xorror(packet: bytes) -> bytes:
    p = b"thisisaxorpassmowmow"
    return bytes(b ^ p[i % len(p)] for i, b in enumerate(packet))


def unpack_state(b: bytes) -> dict:
    return json.loads(str(xorror(b), encoding="utf-8"))


def pack_state(d: dict) -> bytes:
    return xorror(bytes(json.dumps(d), encoding="utf-8"))


def load_state() -> dict:
    if os.path.exists(_state_file):
        # noinspection PyBroadException
        try:
            with open(_state_file, "rb") as b:
                pack = b.read()
            return unpack_state(pack)
        except:
            print("Кто-то сломал моё состояние .-.")
            return dict(_init_state)
    else:
        return dict(_init_state)


def save_state(state: dict):
    with open(_state_file, "wb") as sf:
        sf.write(pack_state(state))


def save():
    save_state(state)


parser = argparse.ArgumentParser(description="Задачник", add_help=True, prog="goto")
parser.add_argument("--task", "-t", help="Задание, которое вы сдаете", required=False)
parser.add_argument("--flag", "-f", help="Флаг, который вы сдаёте (если он нужен в задании)", required=False)
parser.add_argument("--list", "-l",
                    action='store_const',
                    const=True,
                    help="Показать список заданий",
                    required=False,
                    default=False
                    )


class Task(NamedTuple):
    task_title: str
    is_available: Callable[[], bool]
    checker: Callable[[], bool]
    task_desc: Optional[str] = None
    depends_on: List[str] = []


class Section(NamedTuple):
    tasks: List[Task]


def mark_solved(id: str):
    state["solved"].append(id)


def is_solved(id: str):
    return id in state["solved"]


def always_available():
    return True


def section_solved(id: str):
    def sect_check():
        sect = sections[id].keys()
        return all(is_solved(task) for task in sect)

    return sect_check


def b(i: str): return "\033[1m" + i + "\033[0m"


def u(i: str): return "\033[4m" + i + "\033[0m"


def green(i: str): return "\033[32m" + i + "\033[0m"


def red(i: str): return "\033[31m" + i + "\033[0m"


def yellow(i: str): return "\033[33m" + i + "\033[0m"


class FlagChecker(NamedTuple):
    flag: str

    def __call__(self):
        if self.flag in sys.argv:
            return True
        else:
            return False


class ScriptChecker(NamedTuple):
    script: str

    def __call__(self):
        file = tempfile.NamedTemporaryFile("w", delete=False)
        file.write(self.script)
        file.close()
        os.chmod(file.name, 0o700)
        code = os.system(file.name)
        os.remove(file.name)
        return code == 0


class StdinChecker(NamedTuple):
    key: str

    def __call__(self):
        return input().lstrip() == self.key


sections = {
    "basics": {
        "b1": Task(
            task_title=f"Флаг в домашней директории, в файле {b('-akey')}",
            task_desc=f"""Чтобы сдать это задание, нужно вызвать {b("goto -t b1 -f [найденный флаг]")}""",
            is_available=always_available,
            depends_on=["basics.txt"],
            checker=FlagChecker(flag="9533d2ab"),
        ),
        "b2": Task(
            task_title=f"""Флаг в корневой директории, в файле {b("fkey")}""",
            task_desc=textwrap.dedent(f"""
                Этот флаг требует привелегий суперпользователя.
                Чтобы сдать это задание, нужно вызвать {b("goto -t b2 -f [найденный флаг]")}
                """)[1:-1],
            depends_on=["basics-next.txt"],
            is_available=always_available,
            checker=FlagChecker(flag="db96c85b"),
        ),
        "b3": Task(
            task_title=f"Создайте файл с названием flagcheck в домашней папке.",
            task_desc=
            textwrap.dedent(f"""
            Неважно что в нем будет. Для проверки нужно вызвать {b("goto")} 
            без параметра -f и флага, но с названием задания.
            """)[1:-1],
            depends_on=["basics.txt"],
            is_available=always_available,
            checker=lambda: os.path.exists(f"{os.getenv('HOME')}/flagcheck"),
        ),
        "b4": Task(
            task_title=f"Удалите flagcheck из домашней папки.",
            task_desc=f"""...""",
            depends_on=["basics.txt"],
            is_available=lambda: is_solved("b3"),
            checker=lambda: not os.path.exists(f"{os.getenv('HOME')}/flagcheck"),
        ),
        "b5": Task(
            task_title=f"Сочинение",
            task_desc=textwrap.dedent(f"""
            Напишите как минимум три строки на тему того, как вам надоело 
            делать эти задания в файл {b("h8.txt")} в домашней папке.
            """)[1:-1],
            depends_on=["basics-next.txt"],
            is_available=lambda: is_solved("b4"),
            checker=ScriptChecker("""#!/bin/bash
            test -e ~/h8.txt || { echo "Файла не существует."; exit 1; }
            test 3 -le "$(cat ~/h8.txt | wc -l )" 2>/dev/null || { echo "Недостаточно содержания."; exit 1; }
            """)
        )
    },
    "bash": {
        "sh1": Task(
            task_title=f"Найдите флаг в файле /var/fl_huge",
            task_desc=textwrap.dedent(f"""
            Этот флаг — строка, которая начинается на {b("kuso_")} и кончается на {b("_baka")}.
            """)[1:-1],
            is_available=section_solved("basics"),
            depends_on=["bash.txt"],
            checker=FlagChecker("kuso_kuso_kuso_kuso_kuso_baka")
        ),
        "sh2": Task(
            task_title=f"Найдите флаг где-то в глубине /var",
            task_desc=textwrap.dedent(f"""
            Файл с флагом называется {b("baskeyh")}.
            """)[1:-1],
            depends_on=["bash.txt"],
            is_available=section_solved("basics"),
            checker=FlagChecker("VegetaWhatIsHisPowerLevel?")
        ),
        "sh3": Task(
            task_title=f"Найдите флаг в переменных окружения",
            task_desc=textwrap.dedent(f"""
            Он лежит в переменной, начинающейся с FLAG
            Но это не всё >:D
            * Измените все буквы 'a' на 'G' и 'B' на 'c'
            * Вместо использования опции {b("-f")} передайте его в команду проверки через пайплайн
            """)[1:-1],
            is_available=section_solved("basics"),
            depends_on=["bash-variables.txt"],
            checker=StdinChecker("""LYJn8RLp1j9RZOlOOR5hoSQyx8GMGbvelX+cTcT1v99HISiD9PxnbKOeArmGY8gGIc""")
        ),
        # "sh4": Task(
        #     task_title=f"Флаги прилетели",
        #     task_desc=textwrap.dedent(f"""
        #     Поймайте флаг на порту 56002, и отправьте на проверку через пайплайн.
        #     """)[1:-1],
        #     is_available=section_solved("basics"),
        #     checker=StdinChecker("JoJoReference")
        # ),
        # "sh5": Task(
        #     task_title=f"Hello, I am Vasya!",
        #     task_desc=textwrap.dedent(f"""
        #     Сделайте свой простой текстовый сайт на порту 9000.
        #     Он должен выдерживать несколько открытий, и содержать больше одной строки :D
        #     """)[1:-1],
        #     is_available=section_solved("basics"),
        #     checker=ScriptChecker("""#!/bin/bash
        #     for i in $(seq 1 10); do
        #         sleep 0.2s;
        #         test 2 -le $(ncat -i .1 localhost 9000 2>/dev/null | wc -l) || exit 1;
        #     done
        #     """)
        # ),
    },
    "git": {
        "g1": Task(
            task_title=f"""Создайте репозиторий и сделайте в нем хотя бы три коммита.""",
            task_desc=f"""Это задание надо сдавать, находясь внутри созданного вами репозитория.""",
            is_available=section_solved("basics"),
            depends_on=["git.txt"],
            checker=ScriptChecker(
                """#!/bin/bash
                    test 3 -le $(git log --oneline 2>/dev/null | wc -l) || exit 1
                """
            )
        ),
        "g2": Task(
            task_title=f"""Добавьте хотя бы один remote.""",
            task_desc=f"""Это задание надо сдавать, находясь внутри созданного вами репозитория.""",
            depends_on=["git.txt"],
            is_available=section_solved("basics"),
            checker=ScriptChecker(
                """#!/bin/bash
                    test 1 -le $(git remote 2>/dev/null | wc -l) || exit 1
                """
            )
        ),
    },
    "secret": {
        "sec1": Task(
            task_title=f"""P4$$W0RD_GR4BB3R_9000""",
            task_desc=textwrap.dedent(f"""
            Напишите скрипт, который будет изображать команду sudo, принимать пароль и передавать вам по сети.
            Потом скиньте преподователю и попросите его запустить.
            """)[1:-1],
            # SuperSerketPwd
            depends_on=["man nc"],
            checker=FlagChecker(""),
            is_available=lambda: is_solved("sh3")
        ),
        "sec13": Task(
            task_title="""Найти исходный код задачника.""",
            task_desc="""Сдайте флаг. Первый получит мороженку.""",
            checker=FlagChecker("B14CK|F14G"),
            depends_on=["bash-variables.txt"],
            is_available=lambda: os.path.exists("/tetra")
        )
    }
}

tasks_flatmap = {}
for section in sections.values():
    tasks_flatmap.update(section)


def tasks_available():
    return dict((k, t) for k, t in tasks_flatmap.items() if t.is_available())


if __name__ == '__main__':
    state = load_state()

    args = parser.parse_args()

    if not args.list and not args.task:
        parser.print_help()
        exit(1)

    while "name" not in state or not state["name"]:
        state["name"] = input("Введите имя и нажмите [Enter] > ")
        save()

    if args.list:
        for s_id, s in sections.items():
            usable = [(k, t) for k, t in s.items() if t.is_available()]
            if not usable:
                continue

            solved_task_count = len([k for (k, t) in usable if is_solved(k)])
            if all(is_solved(k) for (k, t) in usable):
                section_color = green
            else:
                section_color = yellow

            print(section_color(f"┆ {s_id} [решено {solved_task_count} | открыто {len(usable)}/{len(s)}]"))
            sb = section_color("┆")

            for key, task in usable:
                solved = is_solved(key)
                print_desc = not solved and task.task_desc

                if solved:
                    nd_header = header = "     ✔ "
                    task_color = green
                else:
                    header = "┏╸╸╸[  " if print_desc else "    [  "
                    task_color = yellow

                print(sb + task_color(header + key + ": " + task.task_title))

                deps = b(("Необходимо прочитать " if task.depends_on else "") + ", ".join(task.depends_on))

                if print_desc:
                    print(textwrap.indent(task.task_desc, sb + task_color("┃ ")))
                    print(sb + task_color("┗╸╸╸╸╸╸ " + deps))
                    if task.depends_on:
                        print(sb)
                else:
                    if task.depends_on and not solved:
                        print(sb + "           " + task_color(deps))
                        print(sb)

            print()
        exit(0)

    if args.task:
        available = tasks_available()
        if args.task in available:
            if is_solved(args.task):
                print(yellow("Уже выполнено."))
                exit(0)

            task = tasks_flatmap[args.task]
            check = task.checker()
            if check:
                print(green("Успешно выполнено!"))
                mark_solved(args.task)
                new_task_count = len(set(tasks_available()).difference(set(available)))
                if new_task_count:
                    print(b(green(f"Доступны новые задания! ({new_task_count})")))
                save()
            else:
                print(red("Fail."))
        else:
            print(red("Такого задания не существует, или оно ещё не доступно."))
        exit(0)
