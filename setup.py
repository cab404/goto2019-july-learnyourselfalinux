import os
import os.path
import shutil as sh
import random
import tempfile

if os.getuid() != 0:
    print("run with sudo")
    exit(0)

username = "neo"


def write_file(fname: str, contents: str, mod: int = 0o644, user: str = username):
    with open(fname, "w") as f:
        f.write(contents)
    os.chmod(fname, mod)
    sh.chown(fname, user=username)


def exec_script(script: str):
    f = tempfile.NamedTemporaryFile(mode="w", delete=False)
    f.write(script)
    f.close()
    os.chmod(f.name, 0o700)
    os.system(f.name)
    os.remove(f.name)


# b1
write_file(f"/home/{username}/-akey", "9533d2ab\n")

# b2
write_file("/fkey", "db96c85b\n", mod=0, user="root")

# sh1
random.seed(1)
variants = ["baKa", "Baka", "baka", "ukso", "kusO", "baba"]
lines = []
for i in range(10000):
    random.shuffle(variants)
    lines.append("_".join(variants))
lines.append("kuso_kuso_kuso_kuso_kuso_baka")
random.shuffle(lines)
write_file("/var/fl_huge", "\n".join(lines), user="root")

# sh2
write_file("/var/log/journal/baskeyh", "VegetaWhatIsHisPowerLevel?\n", mod=0, user="root")

# sh3
with open(f"/home/{username}/.bashrc", "a") as f:
    f.write("\nexport FLAG_HERE=LYJn8RLp1j9RZOlOOR5hoSQyx8aMabvelX+BTBT1v99HISiD9PxnbKOeArmGY8gGIc\n")

# # sh4
# write_file("/usr/lib/systemd/system/flag_thrower.service", """
# [Unit]
# Description=NC sh4 solver.
# After=network.target
# [Service]
# ExecStart=bash -c 'while true; do nc -c "echo JoJoReference" localhost 56002; sleep 5s; done'
# Restart=always
# [Install]
# WantedBy=multi-user.target
# """, user="root")
#
# exec_script("""#!/bin/bash
# systemctl daemon-reload
# systemctl enable --now flag_thrower.service
# """)

os.mkdir("/usr/lib/il", mode=0o755)
for f in [
    "./broadcaster.py",
    "./manual.py",
    "./beep",
    "./winxperror.wav"
]:
    sh.copy(f, "/usr/lib/il")
os.mkdir("/usr/lib/theater", mode=0o755)
# state broadcaster

write_file("/usr/lib/systemd/system/ilosos.sh", f"""
[Unit]
Description=IndividuaL Open Status Offering Service (ILOSOS)
After=network.target
[Service]
ExecStart=/usr/lib/il/broadcast.py /home/{username}/.local/.less.bin'
Restart=always
[Install]
WantedBy=multi-user.target
""", user="root")

exec_script("""#!/bin/bash
systemctl daemon-reload
systemctl enable --now ilosos.service
""")

