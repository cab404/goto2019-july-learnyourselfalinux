#!/usr/bin/env bash
# Example beep solution

for i in `seq 1 30`; do {
./beep 100 a______a
sleep .1s
./beep 100 a__________a
sleep .1s
./beep 100 a____________________a
sleep .1s
./beep 100 a__________a
sleep .1s
}
done &
for i in `seq 1 10`; do {
./beep 100 A______A
sleep .4s
./beep 100 A__________A
sleep .4s
./beep 100 A____________________A
sleep .4s
./beep 100 A__________A
sleep .4s
}
done &
for i in `seq 1 4`; do {
./beep error
sleep 1.6s
./beep error
sleep .8s
./beep error
sleep .8s
./beep error
sleep 1.6s
}
done &
