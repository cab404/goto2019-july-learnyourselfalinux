import sys
from asyncio import StreamReader, StreamWriter
from ipaddress import IPv4Address
from typing import Union, Text, Tuple

import manual
import asyncio

import ipaddress


async def open_address(addr: IPv4Address):
    global fcount
    try:
        r, w = await asyncio.wait_for(
            asyncio.open_connection(str(addr), 62814),
            timeout=1
        )
        buf = await r.read()
    except OSError:
        return None
    except asyncio.futures.TimeoutError:
        return None

    try:
        state = manual.unpack_state(buf)
    except:
        return None
    return state


async def main():
    network = sys.argv[1] if len(sys.argv) > 1 else "192.168.0.0/24"
    print(f"Using the {network} range.")
    network = ipaddress.ip_network(network)
    sf = (a for a in await asyncio.gather(*[open_address(a) for a in network]) if a)
    print(list(sf))


if __name__ == '__main__':
    asyncio \
        .get_event_loop() \
        .run_until_complete(main())
