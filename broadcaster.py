import asyncio
from asyncio import StreamWriter, StreamReader
import os

import sys

fname = sys.argv[1]

# THIS IS AN INSECURE WAY OF DOING THAT AND PRONE TO REPLAY ATTACKS
# DO NOT DO THAT

async def handle(r: StreamReader, w: StreamWriter):
    if os.path.exists(fname):
        with open(fname, "rb") as a:
            w.write(a.read())
    w.close()


async def start_server():
    server = await asyncio.start_server(
        client_connected_cb=handle,
        host="0.0.0.0",
        port=62814
    )
    await server.wait_closed()


asyncio.get_event_loop().run_until_complete(start_server())
